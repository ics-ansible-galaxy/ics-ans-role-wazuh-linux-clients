import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('wazuh-linux-clients-centos')


def test_ossec(host):
    wazuh_agent = host.service("wazuh-agent")
    assert wazuh_agent.is_enabled
    assert wazuh_agent.is_running
