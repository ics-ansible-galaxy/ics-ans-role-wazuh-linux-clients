# ics-ans-role-wazuh-linux-clients

Ansible role to install wazuh-linux-clients.

## Role Variables

```yaml
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-wazuh-linux-clients
```

## License

BSD 2-clause
